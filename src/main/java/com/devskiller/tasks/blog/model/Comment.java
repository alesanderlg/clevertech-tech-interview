package com.devskiller.tasks.blog.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class Comment {

	@Id
	@GeneratedValue
	private Long id;

	private Long postId;

	private String author;

	private String content;

	private LocalDateTime creationDate;

	public Long getId() { return id; }

	public void setId(Long id) { this.id = id; }

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getCreationDate() { return creationDate; }

	public void setCreationDate(LocalDateTime creationDate) { this.creationDate = creationDate; }
}
