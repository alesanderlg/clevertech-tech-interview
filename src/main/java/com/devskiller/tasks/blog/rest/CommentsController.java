package com.devskiller.tasks.blog.rest;

import com.devskiller.tasks.blog.error.PostNotFoundException;
import com.devskiller.tasks.blog.model.dto.CommentDto;
import com.devskiller.tasks.blog.model.dto.NewCommentDto;
import com.devskiller.tasks.blog.model.dto.PostDto;
import com.devskiller.tasks.blog.service.CommentService;
import com.devskiller.tasks.blog.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Controller
@RestController
@RequestMapping("/posts/")
public class CommentsController {

	private CommentService commentService;
	private PostService postService;

	@Autowired
	public CommentsController(CommentService commentService, PostService postService){
		this.commentService = commentService;
		this.postService = postService;
	}

	@PostMapping(value = "{postId}/comments")
	public ResponseEntity<Object> postComment(@PathVariable Long postId, @RequestBody NewCommentDto newCommentDto){

		if(!postService.exists(postId)){
			throw new PostNotFoundException("Post not found");
		}

		final Long id = commentService.addComment(newCommentDto);
		URI location = ServletUriComponentsBuilder
			.fromCurrentRequest()
			.path("/comments/{id}")
			.buildAndExpand(id).toUri();
		return ResponseEntity.created(location).build();
	}

	@GetMapping(value = "{postId}/comments")
	@ResponseStatus(HttpStatus.OK)
	public List<CommentDto> getComments(@PathVariable Long postId){
		return commentService.getCommentsForPost(postId);
	}

}
